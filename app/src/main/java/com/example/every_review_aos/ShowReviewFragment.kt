package com.example.every_review_aos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment

class ShowReviewFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_show_review, null)

        val bundle = arguments

        var nickname = bundle!!.getString("NICKNAME")
        var rating = bundle!!.getDouble("RATING")
        var content = bundle!!.getString("CONTENT")


        val review_name = view.findViewById<TextView>(R.id.review_name)
        val review_content = view.findViewById<TextView>(R.id.review_content)

        review_name.setText(nickname)
        review_content.setText(content)

        val backButton = view.findViewById<ImageButton>(R.id.back_button)

        backButton.setOnClickListener {
            fragmentManager!!.beginTransaction().remove(this).commit()
            fragmentManager!!.popBackStack()
        }



        return view
    }
}