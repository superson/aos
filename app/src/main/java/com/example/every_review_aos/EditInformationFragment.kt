package com.example.every_review_aos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment

class EditInformationFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_edit_information, null)

        val backButton = view.findViewById<ImageButton>(R.id.back_button)
        val saveButton = view.findViewById<Button>(R.id.save_button)

        backButton.setOnClickListener {
            fragmentManager!!.beginTransaction().remove(this).commit()
            fragmentManager!!.popBackStack()
        }

        saveButton.setOnClickListener {
            fragmentManager!!.beginTransaction().remove(this).commit()
            fragmentManager!!.popBackStack()
        }

        return view
    }
}