package com.example.every_review_aos

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import org.json.JSONObject
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class LogInFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, null)

        val id_edit = view.findViewById<EditText>(R.id.id_edit)
        val pw_edit = view.findViewById<EditText>(R.id.pw_edit)


        val login_button = view.findViewById<Button>(R.id.login_button)

        login_button.setOnClickListener {


            try {
                val thread = Thread({

                    val url = URL("http://52.79.175.114:8000/account/login/")

                    Log.d("CHOSH", url.toString())

                    val connection: HttpURLConnection = url.openConnection() as HttpURLConnection

                    connection.setRequestProperty("Content-Type", "application/json")

                    val json = JSONObject()

                    json.put("username", id_edit.text.toString())
                    json.put("password", pw_edit.text.toString())

                    connection.requestMethod = "POST"
                    connection.doInput = true
                    connection.doOutput = true
                    connection.useCaches = false

                    val out = OutputStreamWriter(connection.outputStream)
                    out.write(json.toString())
                    out.close()

                    connection.connect()

                    val code = connection.responseCode


                    if (code == 200) {



                        Log.d("CHOSH", "Login Post Request Succeed")

                        val cookies = connection.headerFields.get("Set-Cookie")

                        if (cookies != null) {
                            Log.d("CHOSH", cookies.toString())

                            val sessionid = cookies[1].split(";")[0]

                            Log.d("CHOSH", sessionid)

                            val pref = context!!.getSharedPreferences("sessionCookie", Context.MODE_PRIVATE)
                            val edit = pref.edit()

                            if (pref.getString("sessionid", null) == null) {
                                Log.d("CHOSH", "login session input first" + sessionid)
                            } else if (!pref.getString("sessionid", null).equals(sessionid)) {
                                Log.d("CHOSH", "change session id " + sessionid)
                            }

                            edit.putString("sessionid", sessionid)
                            edit.apply()

                        }


                        fragmentManager!!.beginTransaction().remove(this).commit()
                        fragmentManager!!.popBackStack()

                        connection.disconnect()

                    } else {
                        connection.disconnect()
                        Log.d("CHOSH", "Connection Error")
                        Log.d("CHOSH", code.toString())
                    }





                })

                thread.start()
                thread.join()
            } catch(e: Throwable) {
                e.printStackTrace()
                Log.d("CHOSH", e.toString())
            }




        }

        return view
    }
}