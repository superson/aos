package com.example.every_review_aos


data class ReviewVO(val id: Int, val nickname: String, val rating: Double, val date: Double, val content: String, val like_cnt: Int, val ref_name: String?, val ref_url: String?, var is_liked: Boolean)