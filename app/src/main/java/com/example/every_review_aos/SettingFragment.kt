package com.example.every_review_aos

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class SettingFragment: Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_setting, null)

        val cell0 = view.findViewById<LinearLayout>(R.id.cell0)
        val cell1 = view.findViewById<LinearLayout>(R.id.cell1)
        val cell2 = view.findViewById<LinearLayout>(R.id.cell2)

        val login_text = view.findViewById<TextView>(R.id.login_text)

        val pref = context!!.getSharedPreferences("sessionCookie", Context.MODE_PRIVATE)
        val sessionid = pref.getString("sessionid", null)

        if (sessionid != null) {
            login_text.setText("로그아웃")
        }


        cell0.setOnClickListener {
            if (sessionid == null) {
                fragmentManager!!.beginTransaction().replace(R.id.fragment, LogInFragment()).addToBackStack(null)
                    .commit()
            } else {

                try {
                    val thread = Thread({

                        val url = URL("http://52.79.175.114:8000/account/logout/")

                        Log.d("CHOSH", url.toString())

                        val connection: HttpURLConnection = url.openConnection() as HttpURLConnection

                        connection.setRequestProperty("Content-Type", "application/json")
                        connection.setRequestProperty("Cookie", sessionid)

                        connection.requestMethod = "POST"
                        connection.doInput = true
                        connection.doOutput = true
                        connection.useCaches = false

                        connection.connect()

                        val code = connection.responseCode


                        if (code == 200) {



                            Log.d("CHOSH", "Logout Post Request Succeed")


                            val edit = pref.edit()
                            edit.remove("sessionid")
                            edit.apply()


                            fragmentManager!!.beginTransaction().remove(this).commit()
                            fragmentManager!!.popBackStack()

                            connection.disconnect()

                        } else {
                            connection.disconnect()
                            Log.d("CHOSH", "Connection Error")
                            Log.d("CHOSH", code.toString())
                        }





                    })

                    thread.start()
                    thread.join()
                } catch(e: Throwable) {
                    e.printStackTrace()
                    Log.d("CHOSH", e.toString())
                }








            }

        }


        return view
    }


}