package com.example.every_review_aos

import android.app.Activity
import android.content.Context
import android.icu.text.IDNA
import android.media.Image
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class WebviewFragment: Fragment() {

    val list = ArrayList<ReviewVO>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_webview, null)

        val bundle = arguments
        val reviewTitle = bundle!!.getString("TITLE")

        val backButton = view.findViewById<ImageButton>(R.id.back_button)

        backButton.setOnClickListener {
            fragmentManager!!.beginTransaction().remove(this).commit()
            fragmentManager!!.popBackStack()
        }

        val title = view.findViewById<TextView>(R.id.title)

        title.setText(reviewTitle)
        val webview = view.findViewById<WebView>(R.id.webview)

        val settings = webview.settings

        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        webview.setWebViewClient(WebViewClient())
        val headers: Map<String, String> = mapOf(Pair("X-ER-DEVICE" ,"android-application"))

        webview.loadUrl("http://52.79.175.114:8000/wiki/pages/" + reviewTitle, headers)
//        webview.loadUrl("http://www.apple.com/")

        try {
            val thread = Thread({

                val url = URL("http://52.79.175.114:8000/review/pages/" + reviewTitle + "/")

                Log.d("CHOSH", url.toString())

                val connection: HttpURLConnection = url.openConnection() as HttpURLConnection

                connection.setRequestProperty("Content-Type", "application/json")
                val pref = context!!.getSharedPreferences("sessionCookie", Context.MODE_PRIVATE)
                val sessionid = pref.getString("sessionid", null)

                if (sessionid != null) {
                    Log.d("CHOSH", "include session id")
                    connection.setRequestProperty("Cookie", sessionid)
                }

                connection.connect()


                val code = connection.responseCode


                if (code == 200) {

                    val responseBody = connection.inputStream

                    val responseBodyReader = InputStreamReader(responseBody, "UTF-8")

                    val br = BufferedReader(responseBodyReader)

                    val json = br.readLine()

                    Log.d("CHOSH", json)

                    val jsonObject = JSONObject(json)

                    val reviews = jsonObject.getJSONArray("reviews")

                    Log.d("CHOSH", reviews.toString())

                    for (i in 0 until reviews.length()) {
                        val review = reviews[i] as JSONObject

                        val id = review.getInt("id")
                        val nickname = review.getString("nickname")
                        val rating = review.getDouble("rating")
                        val content = review.getString("content")
                        val date = review.getDouble("date")
                        val like_cnt = review.getInt("like_cnt")
                        val ref_name = review.optString("ref_name")
                        val ref_url = review.optString("ref_url")
                        val is_liked = review.getBoolean("is_liked")

                        val reviewvo = ReviewVO(id, nickname, rating, date, content, like_cnt, ref_name, ref_url, is_liked)

                        Log.d("CHOSH", reviewvo.toString())

                        list.add(reviewvo)
                    }

                    connection.disconnect()

                } else {
                    connection.disconnect()
                    Log.d("CHOSH", "Connection Error")
                    Log.d("CHOSH", code.toString())
                }





            })

            thread.start()
            thread.join()
        } catch(e: Throwable) {
            e.printStackTrace()
            Log.d("CHOSH", e.toString())
        }





        val reviewList = view.findViewById<RecyclerView>(R.id.review_list)

        reviewList.adapter = ReviewAdapter(list, fragmentManager!!, context!!)
        reviewList.layoutManager = LinearLayoutManager(this.context)

        val reviewSortingButton = view.findViewById<Button>(R.id.review_sorting_button)

        reviewSortingButton.setOnClickListener {

            val builder = AlertDialog.Builder(this.context!!)

            val dialogView = layoutInflater.inflate(R.layout.sorting_dialog, null)

            builder.setView(dialogView)


            val alert = builder.create()

            alert.show()
        }









        val edit_review_button = view.findViewById<Button>(R.id.right_button)


        edit_review_button.setOnClickListener {

            val bundle = Bundle()

            bundle.putString("TITLE", reviewTitle)

            val fragment = EditReviewFragment()
            fragment.arguments = bundle

            fragmentManager!!.beginTransaction().replace(R.id.fragment,fragment).addToBackStack(null).commit()
        }










        return view
    }

    class ReviewAdapter(val dataList: ArrayList<ReviewVO>, val fragmentManager: FragmentManager, val context: Context): RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

        class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {


            val review_item = itemView.findViewById<ConstraintLayout>(R.id.review_item) // 클릭시 리뷰 보이게 하는 용

            val reviewNickname = itemView.findViewById<TextView>(R.id.review_nickname)
            val reviewDate = itemView.findViewById<TextView>(R.id.review_date)
            val reviewReference = itemView.findViewById<TextView>(R.id.review_reference)
            val reviewContent = itemView.findViewById<TextView>(R.id.review_content)


            val review_like = itemView.findViewById<ImageButton>(R.id.review_like)
            val review_like_count = itemView.findViewById<TextView>(R.id.review_like_count)

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewAdapter.ViewHolder {
            val inflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.review_item, parent, false)

            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return dataList.size
        }

        override fun onBindViewHolder(holder: ReviewAdapter.ViewHolder, position: Int) {
            val id = dataList.get(position).id
            val nickname = dataList.get(position).nickname
            val date = dataList.get(position).date
            val reference = dataList.get(position).ref_name

            val rating = dataList.get(position).rating
            val content = dataList.get(position).content

            val timeStamp = Date(date.toLong() *1000)
            val timeFormat = SimpleDateFormat("yyyy-MM-dd").format(timeStamp)


            holder.reviewNickname.setText(nickname)
            holder.reviewDate.setText(timeFormat)
            if (reference != "") {
                holder.reviewReference.setText("출처 : " + reference)
            } else {
                holder.reviewReference.setText("")
            }

            val is_liked = dataList.get(position).is_liked
            holder.review_like_count.setText(dataList.get(position).like_cnt.toString())

            var like_count = dataList.get(position).like_cnt

            if (is_liked) {
                holder.review_like.setBackgroundResource(R.drawable.ic_like_on)
            } else {
                holder.review_like.setBackgroundResource(R.drawable.ic_like_off)
            }



            holder.reviewContent.setText(content)

            holder.review_item.setOnClickListener {

                val bundle = Bundle()
                bundle.putString("NICKNAME", nickname)
                bundle.putDouble("RATING", rating)
                bundle.putString("CONTENT", content)

                val fragment = ShowReviewFragment()
                fragment.arguments = bundle

                fragmentManager.beginTransaction().add(R.id.fragment, fragment).addToBackStack(null).commit()
            }


            holder.review_like.setOnClickListener {

                if (is_liked) {
                    it.setBackgroundResource(R.drawable.ic_like_off)
                    dataList.get(position).is_liked = false
                    like_count -= 1
                } else {
                    it.setBackgroundResource(R.drawable.ic_like_on)
                    dataList.get(position).is_liked = true
                    like_count += 1

                }

                holder.review_like_count.setText(like_count.toString())

                try {
                    val thread = Thread({

                        val url = URL("http://52.79.175.114:8000/review/like/" + id + "/")

                        Log.d("CHOSH", url.toString())

                        val connection: HttpURLConnection = url.openConnection() as HttpURLConnection

                        connection.setRequestProperty("Content-Type", "application/json")

                        val pref = context!!.getSharedPreferences("sessionCookie", Context.MODE_PRIVATE)
                        val sessionid = pref.getString("sessionid", null)

                        if (sessionid != null) {
                            Log.d("CHOSH", "include session id")
                            connection.setRequestProperty("Cookie", sessionid)
                        }


                        val json = JSONObject()

                        json.put("id", id)
                        json.put("is_liked", dataList.get(position).is_liked)


                        connection.requestMethod = "POST"
                        connection.doInput = true
                        connection.doOutput = true
                        connection.useCaches = false

                        val out = OutputStreamWriter(connection.outputStream)
                        out.write(json.toString())
                        out.close()

                        connection.connect()

                        val code = connection.responseCode


                        if (code == 200) {

                            Log.d("CHOSH", "Post Request Succeed")


                            connection.disconnect()

                        } else {
                            connection.disconnect()
                            Log.d("CHOSH", "Connection Error")
                            Log.d("CHOSH", code.toString())
                        }





                    })

                    thread.start()
                } catch(e: Throwable) {
                    e.printStackTrace()
                    Log.d("CHOSH", e.toString())
                }




            }


        }


    }

}
