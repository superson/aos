package com.example.every_review_aos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment


class HomeFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_home, null)



        val searchEditText = view.findViewById<EditText>(R.id.search)
        val searchButton = view.findViewById<Button>(R.id.search_btn)


        searchButton.setOnClickListener {

            val bundle = Bundle()
//            bundle.putString("TITLE", searchEditText.text.toString())
            bundle.putString("TITLE", "기생충")

            val fragment = WebviewFragment()
            fragment.arguments = bundle

            fragmentManager!!.beginTransaction().add(R.id.fragment, fragment).addToBackStack(null).commit()

        }


        return view
    }


}

