package com.example.every_review_aos

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import org.w3c.dom.Text
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.nio.Buffer


class EditReviewFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_edit_review, null)

        val backButton = view.findViewById<ImageButton>(R.id.back_button)
        val saveButton = view.findViewById<Button>(R.id.save_button)

        val review_content = view.findViewById<TextView>(R.id.review_content)


        val bundle = arguments
        val reviewTitle = bundle!!.getString("TITLE")


        backButton.setOnClickListener {
            fragmentManager!!.beginTransaction().remove(this).commit()
            fragmentManager!!.popBackStack()
        }

        saveButton.setOnClickListener {


            try {
                val thread = Thread({

                    val url = URL("http://52.79.175.114:8000/review/pages/" + reviewTitle + "/")

                    Log.d("CHOSH", url.toString())

                    val connection: HttpURLConnection = url.openConnection() as HttpURLConnection

                    connection.setRequestProperty("Content-Type", "application/json")

                    val pref = context!!.getSharedPreferences("sessionCookie", Context.MODE_PRIVATE)
                    val sessionid = pref.getString("sessionid", null)

                    if (sessionid != null) {
                        Log.d("CHOSH", "include session id")
                        connection.setRequestProperty("Cookie", sessionid)
                    }


                    val json = JSONObject()

                    json.put("nickname", "조수형")
                    json.put("rating", "4.5")
                    json.put("content", review_content.text.toString())

                    connection.requestMethod = "POST"
                    connection.doInput = true
                    connection.doOutput = true
                    connection.useCaches = false

                    val out = OutputStreamWriter(connection.outputStream)
                    out.write(json.toString())
                    out.close()

                    connection.connect()

                    val code = connection.responseCode


                    if (code == 200) {

                        Log.d("CHOSH", "Post Request Succeed")

                        fragmentManager!!.beginTransaction().remove(this).commit()
                        fragmentManager!!.popBackStack()

                        connection.disconnect()

                    } else {
                        connection.disconnect()
                        Log.d("CHOSH", "Connection Error")
                        Log.d("CHOSH", code.toString())
                    }





                })

                thread.start()
                thread.join()
            } catch(e: Throwable) {
                e.printStackTrace()
                Log.d("CHOSH", e.toString())
            }




        }


        return view
    }
}